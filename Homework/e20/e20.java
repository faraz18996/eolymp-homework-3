import java.util.Scanner;

public class e20 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();

        int count = 0;
        while (n > 0) {
            int temp = n;
            int sum = 0;
            while (temp > 0) {
                sum =sum + temp % 10;
                temp =temp / 10;

            }

            n =n - sum;
            count++;
        }

        System.out.println( count);
    }
}